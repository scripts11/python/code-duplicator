# -*- coding: utf-8 -*-
"""
Created on Sat Jul  4 21:25:25 2020

@author: Luke
"""
import sys, getopt

## Main function
def main(argv):
  #Default input/ouput names
  inputfile = 'input.txt'
  outputfile = 'output.txt'
  
  keys = {}
  
  try:
    opts, args = getopt.getopt(argv,"hi:o:",["ifile=","ofile=","key="])
  except getopt.GetoptError:
    print('test.py -i <inputfile> -o <outputfile>')
    sys.exit(2)
  for opt, arg in opts:
    if opt == '-h':
      print('test.py -i <inputfile> -o <outputfile>')
      sys.exit()
    elif opt in ("-i", "--ifile"):
      inputfile = arg
    elif opt in ("-o", "--ofile"):
      outputfile = arg

    
  file_in = open(inputfile, 'r')
  file_out = open(outputfile, 'w')
  
  #Parse key values
  while True:
    line = file_in.readline()
    if line == '\n':
      break
    keys[(line.split(':')[0])] = line.split(':')[-1].strip('\n').split(',')
    #Reverse order
    keys[(line.split(':')[0])].reverse()
  
  # Move to where code block begins and step back one char
  while file_in.read(1) == '\n':
    continue
  file_in.seek(file_in.tell()-1)
  content = file_in.read()
  
  #Change content
  #While the number of values in keys[key] is greater than 0
  while (len(keys[next(iter(keys))]) > 0):
    change_string = content;
    for key, value in keys.items():
      try:
        change_string = change_string.replace(key, value.pop())
      except IndexError as error:
        print(key, ": value array too short")
        return
    file_out.write(change_string)
                  
  file_in.close()
  file_out.close()
    
##Run Main Function
if __name__ == "__main__":
    main(sys.argv[1:])